# Architech Exercise #

a simple web app for user registeration

### Required ###

* Instance of MongoDb must be running locally with default port. ie localhost:27017


### Tools ###

* Java 7
* Maven 3.2.3
* Tomcat 7 (embedded)
* MongoDB 2.6.4
* Bootstrap 3.2.0

### Run ###

In the project's root folder simply run

```
#!bash

mvn clean tomcat7:run-war
```

access the web app locally with port 8080

[localhost:8080](localhost:8080)