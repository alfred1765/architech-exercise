<%@page import="com.alfred.architech.model.User"%>
<%@page import="com.alfred.architech.utils.WebUtils"%>
<%
	String func = WebUtils.getString("func", "", request);
	boolean registering = false;
	String errMsg = "";
	if (func.equals("register")) {
		registering = true;
		String username = WebUtils.getString("username", null, request);
		String password = WebUtils.getString("password", null, request);
		
		errMsg = User.validateAndRegister(username, password);
	}
%>

<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="author" content="">
	<link rel="icon" href="http://getbootstrap.com/favicon.ico">
	
	<title>Signin</title>
	
	<!-- Bootstrap core CSS -->
	<link href="css/bootstrap.min.css" rel="stylesheet">
	
	<!-- Custom styles for this template -->
	<link href="css/signin.css" rel="stylesheet">
</head>

<body>

	<div class="container">

		<%if (registering && errMsg == null) { %>
			<div class="alert alert-success">
				<a href="/" class="close" data-dismiss="alert">&times;</a> 
				<strong>Success!</strong> You have successfully registered.
			</div>
		<%
		} else {
		%>
		<%if (errMsg != null && !errMsg.isEmpty()) { %>
			<div class="alert alert-danger alert-error">
				<a href="/" class="close" data-dismiss="alert">&times;</a> 
				<strong>Error!</strong> <%=errMsg%>.
			</div>
		<%
		}
		%>
			<form class="form-signin" role="form" method="post" action="/">
				<h2 class="form-signin-heading">Please register</h2>
				<input name="func" type="hidden" value="register">
				<input name="username" type="text" class="form-control" placeholder="Username"	required="" autofocus=""> 
				<input name="password" type="password" class="form-control" placeholder="Password" required="">
				<button class="btn btn-lg btn-primary btn-block" type="submit">Register</button>
			</form>
		<%} %>

	</div>
	<!-- /container -->
</body>
</html>