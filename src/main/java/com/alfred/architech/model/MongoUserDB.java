package com.alfred.architech.model;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.MongoClient;
import com.mongodb.WriteConcern;

public class MongoUserDB implements PersistUserStore {

	private static String DB_HOST = "127.0.0.1";
	private static int DB_PORT = 27017;
	private static String DB_DATABASE = "test";
	private static String DB_COLLECTION = "users";
	
	private enum MongoSingleton {
		INSTANCE;
		private MongoClient mongoClient;
		
		private MongoSingleton() {
			try {
				mongoClient = new MongoClient(DB_HOST, DB_PORT);
			} catch (Exception e) {
				e.printStackTrace();
				mongoClient = null;
			}
		}
		
		public MongoClient getClient() {
			return mongoClient;
		}
	}
	
	public MongoUserDB(){}
	
	@Override
	public boolean exists(String username) {
		DB db = getClient().getDB(DB_DATABASE);
		DBCollection coll = db.getCollection(DB_COLLECTION);
		BasicDBObject query = new BasicDBObject("username", username);
		DBCursor result = coll.find(query);
		return result.hasNext();
	}

	@Override
	public User register(String username, String password) {
		DB db = getClient().getDB(DB_DATABASE);
		DBCollection coll = db.getCollection(DB_COLLECTION);
		BasicDBObject doc = new BasicDBObject();
		doc.put("username", username);
		doc.put("password", password);
		try {
			coll.insert(doc, WriteConcern.SAFE);
			return new User(username, password);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	@Override
	public boolean initialized() {
		MongoClient client = MongoSingleton.INSTANCE.getClient();
		return client != null;
	}
	
	private MongoClient getClient() {
		if (!initialized()) {
			throw new IllegalStateException("Must be initialized before usage");
		}
		return MongoSingleton.INSTANCE.getClient();
	}
}
