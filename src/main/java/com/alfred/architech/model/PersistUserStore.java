package com.alfred.architech.model;

public interface PersistUserStore {

	boolean initialized();
	boolean exists(String username);
	User register(String username, String password);
}
