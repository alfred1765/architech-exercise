package com.alfred.architech.model;

import org.apache.commons.lang3.StringUtils;

public class User {
	
	public static PersistUserStore getStore() {
		// first try mongodb
		MongoUserDB mongo = new MongoUserDB();
		if (mongo.initialized()) {
			return mongo;
		} 
		throw new IllegalStateException("failed to find proper user store");
	}
	
	public static String validateAndRegister(String username, String password) {
		if (username == null || username.isEmpty()) {
			return "Empty username";
		}
		if (password == null || password.isEmpty()) {
			return "Empty password"; 
		}
		if (!StringUtils.isAlphanumeric(username)) {
			return "Username must be alpha-numeric values";
		}
		if (username.length() < 5) {
			return "Username must be no less than 5 characters";
		}
		PersistUserStore store = getStore();
		if (store.exists(username)) {
			return "Username is already registered";
		}
		
		if (password.length() < 8) {
			return "Password must be longer than 8 characters";
		}
		
		boolean passHasNum = false;
		boolean passHasUpper = false;
		boolean passHasLower = false;
		for (char c : password.toCharArray()) {
			String s = c+"";
			if (!passHasNum && StringUtils.isNumeric(s))
				passHasNum = true;
			if (!passHasUpper && StringUtils.isAllUpperCase(s))
				passHasUpper = true;
			if (!passHasLower && StringUtils.isAllLowerCase(s)) 
				passHasLower = true;
		}
		if (!(passHasLower && passHasUpper && passHasLower)) {
			return "Password must contain at least 1 number, 1 uppercase, and 1 lowercase character";
		}
		User register = store.register(username, password);
		return null;
	}
	
	private String username;
	private String password;
	
	protected User(String username, String password) {
		this.username = username;
		this.password = password;
	}
	
	public String getUsername() {
		return username;
	}

	public String getPassword() {
		return password;
	}
}
