package com.alfred.architech.utils;

import javax.servlet.http.HttpServletRequest;

public class WebUtils {
	
	public static String getString(String name, String defaultValue, HttpServletRequest request) {
		if (name == null) {
			throw new IllegalArgumentException("parameter name is null");
		}
		String value = request.getParameter(name);
		if (value == null) {
			return defaultValue;
		}
		return value;
	}
}
